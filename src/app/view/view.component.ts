import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  Images: [];

  constructor(private dataService: DataService, private router: Router) {
    this.dataService.setActiveUrl('view');
  }

  ngOnInit() {
    this.Images = JSON.parse(localStorage.getItem('imgsData'));
  }

  getStyles(image = null) {
    return {
      backgroundImage: image ? `url(${image.secure_url})` : 'url(https://img.icons8.com/ios/420/add.png)',
      backgroundSize: image ? 'contain' : '20%',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
    };
  }

  goToUploadPage() {
    this.router.navigate(['upload']);
  }
}
