import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private _activePage: BehaviorSubject<any>;
  activePage: any;

  constructor() {
    this._activePage = <BehaviorSubject<any>>new BehaviorSubject([]);
    this.activePage = this._activePage.asObservable();
  }

  setActiveUrl(newPage: string) {
    this._activePage.next(newPage);
  }

  uploadPhoto(file) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', 'x1pirxbq');

    return axios({
      url: 'https://api.cloudinary.com/v1_1/dcwukua6y/image/upload',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: formData,
    });
  }

  saveImageLink(imgData) {
    if (localStorage.getItem('imgsData')) {
      const imgsDataArr = JSON.parse(localStorage.getItem('imgsData'));
      imgsDataArr.push(imgData);
      localStorage.setItem('imgsData', JSON.stringify(imgsDataArr));
    } else {
      localStorage.setItem('imgsData', JSON.stringify([imgData]));
    }
  }
}
