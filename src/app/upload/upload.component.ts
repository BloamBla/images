import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  previewImages: {
    url: string,
  }[];
  Images: any;
  showGoToViewButton: Boolean = false;
  imageBase64: any;
  croppedImage: number;
  croppedImageData: any;

  constructor(private dataService: DataService, private router: Router) {
    this.dataService.setActiveUrl('upload');
    this.previewImages = [];
    this.Images = [];
  }

  ngOnInit() { }

  imageCropped(event) {
    this.croppedImageData = event.base64;
  }

  cropImage(i, image) {
    if (this.croppedImage === i) {
      this.Images[i] = this.croppedImageData;
      return this.croppedImage = null;
    }
    this.imageBase64 = typeof this.Images[i] === 'string' ? this.Images[i] : image.url;
    this.croppedImage = i;
  }

  isCropImage(i) {
    return this.croppedImage === i;
  }

  getSource(i, image) {
    if (typeof this.Images[i] === 'string') {
      return this.Images[i];
    }
    return image.url;
  }

  onFileChange(e) {
    const files = e.target.files;
    if (files.length === 0) {
      return;
    }
    for (let file of files) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.previewImages.push({
          url: event.target.result,
        });
        file = event.target.result;
      };
      reader.readAsDataURL(file);
    }
    if (this.Images && this.Images.length > 0) {
      this.Images = this.Images.concat(Array.from(files));
    } else {
      this.Images = Array.from(files);
    }
  }

  uploadPhotos() {
    this.Images.forEach((file, i) => {
      this.dataService.uploadPhoto(file).then((res) => {
        this.dataService.saveImageLink(res.data);
        if (i === this.Images.length - 1) {
          this.showGoToViewButton = true;
        }
      });
    });
  }

  goToView() {
    this.router.navigate(['view']);
  }

  removePhoto(index) {
    this.previewImages.splice(index, 1);
    this.Images.splice(index, 1);
    const input: any = document.querySelector('#fileInput');
    input.value = '';
  }
}
