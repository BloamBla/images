import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: String = 'image-uploader-viewer';
  activePage: string;

  constructor(private dataService: DataService, private router: Router) {
    this.dataService.activePage.subscribe((activePage) => this.activePage = activePage ? activePage : 'upload');
  }

  ngOnInit() {
  }

  isActiveUrl(url) {
    return this.activePage === url;
  }

  goToPage(page) {
    this.dataService.setActiveUrl(page);
    this.router.navigate([page]);
  }
}
